/*
* This file contains all methods that will be used in the package
/

package bonuspackage;

/**
 * @author ron-friedman
 */

public class Question {
    String helloworld;
    
    /**
     * Constructor with helloworld as its parameter
     * @param helloworld
     */
    
    public Question(String helloworld){
        this.helloworld = helloworld;
    }
    
    /**
     * Mutator that can set a string
     * @param helloworld
     */
    void setHelloWorld(String helloworld){
        this.helloworld = helloworld;
    }
}