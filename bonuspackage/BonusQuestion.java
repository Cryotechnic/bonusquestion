/*
 * Tests methods from Question.java file
 */
 
package bonuspackage;

/**
 * @author ron-friedman
 * 
 */

public static void main(String[] args) {
    // Creates object
    Question firstTest = new helloworld("Hello World!);
    System.out.println("The sentence is: " + firstTest.helloWorld);
    
    // Using the method
    firstTest.setHelloWorld("Hello World again!");
    System.out.println("The sentence after modification becomes: " + firstTest.helloWorld);
 